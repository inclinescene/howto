# howto: An index of tutorial resources for the demoscene

Presented by [Incline](https://demozoo.org/groups/65997/)

Special thanks to guardian for hosting us at <http://howto.planet-d.net>!

No warranty!

Copyright (c) 2016 Chris White.
CC-BY-SA 3.0 for now.

