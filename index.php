<?php

namespace Howto;

define('SITE_ROOT', __DIR__ . DIRECTORY_SEPARATOR);

require SITE_ROOT . 'vendor/autoload.php';
require SITE_ROOT . 'config.php';

use ZendSearch\Lucene\Lucene;
use ZendSearch\Lucene\Document as LDoc;
use ZendSearch\Lucene\Document\Field as LField;
use ZendSearch\Lucene\Index\Term as LTerm;

class HApp extends \JTSM\App {
    // Template locations, with respect to SITE_ROOT/skins
    private $TMPL_ROOT = 'sbadmin/twig/root.twig.php';
    private $TMPL_RESULTS = 'sbadmin/twig/results.twig.php';
    private $EXTERNAL_LINK_ICON = '&nbsp;<i class="fa fa-external-link"></i>';
    /*
    // HTTP
    protected $request;
    protected $response;
    protected $emitter;

    // Routing
    private $collector;
    private $dispatcher;
    protected $generator;

    // Templating
    private $twig;
    */

    // === Setup ===========================================================

    function site_init() {
        $globals = [
            'short_title' => 'Howto',
            'skin' => '/skins/sbadmin', //w.r.t. SITE_ROOT since it's used
                                        //in absolute-URL generation.
            'topitems' => [
                [
                    'class' => 'fa-trophy',
                    'a_extra_inner' => 'aria-hidden:true;',
                    'a_extra' => '<span style="position: absolute !important;clip: rect(1px, 1px, 1px, 1px);">About</span>',
                        // Thanks to http://webaim.org/techniques/css/invisiblecontent/
                    'entries' => [
                        '<a href="' . $this->generateRoute('credz') . '">Credits</a>',
                        '<a href="' . $this->generateRoute('greetz') . '">Greetings</a>',
                    ],
                ],

                [
                    'class' => 'fa-question-circle',
                    'a_extra_inner' => 'aria-hidden:true;',
                    'a_extra' => '<span style="position: absolute !important;clip: rect(1px, 1px, 1px, 1px);">Help</span>',
                        // Thanks to http://webaim.org/techniques/css/invisiblecontent/
                    'entries' => [
                        '<a href="https://framework.zend.com/manual/1.12/en/zend.search.lucene.query-language.html">Query language (Zend Lucene)' . $this->EXTERNAL_LINK_ICON . '</a>',
                        '<a href="https://www.pouet.net/topic.php?which=10882">The inspiration' . $this->EXTERNAL_LINK_ICON . '</a>',
                    ],
                ],
            ],

            'sideitems' => [
                [ 'text' => 'Search',
                  'href' => $this->generateRoute('root'),
                  'icon_class' => 'fa-search' ],
                /* TODO add category browse
                [ 'text' => 'Search',
                  'href' => $this->generateRoute('search'),
                  'icon_class' => 'fa-search' ],
                */
            ], // sideitems
        ];

        foreach($globals as $k => $v) {
            $this->twig->addGlobal($k, $v);
        }
    } //site_init()

    // === Pages ===========================================================

    /**
     * @route "/"
     * For some reason, route / (without the "") doesn't parse - Notoj
     * grabs the at-route tag, but with an empty value.
     */
    function root() {
        $vars = [
            'title' => 'Howto: demoscene tutorial and resource index',

            'content' => [
                'title' => 'Search',
                // no subtitle
            ],

            // no crumbs
        ]; //$vars

        $this->response->getBody()->write(
            $this->twig->render($this->TMPL_ROOT, $vars)
        );
    } //root()

    /**
     * @route /results
     */
    function results() {
        $vars = [
            'title' => 'Search results - Howto',
            'content' => [
                'title' => 'Results',
                // no subtitle
            ],

            //'crumbs' => [
            //    [   'icon_ref' => 'fa-search',
            //        'text' => 'Search',
            //        'href' => $this->generateRoute('root')
            //            // TODO put query text into the breadcrumb
            //    ]       // and then uncomment the crumbs
            //]

            'search_label' => 'Search again',

        ]; //$vars

        $parms = $this->request->getQueryParams();

        $q = $this->QPs('q');
        if(!is_null($q) && (strlen($q)>0)) {
            $vars['query'] = $q;

            // Do the search
            global $fti_path;   //from config.php
            $index = Lucene::open($fti_path);
            $hits = $index->find($q);
            $vars['hits'] = $hits;
        } //endif got a query

        $this->response->getBody()->write(
            $this->twig->render($this->TMPL_RESULTS,$vars));
    } //results()

    /**
     * @route /credz
     */
    function credz() {
        $vars = [
            'title' => 'Credits - Howto',
            'nosearch' => true,

            'content' => [
                'title' => 'Credits',
                // no subtitle
            ],

            'rows' => [
                <<<EOCREDZ
<ul>
<li>Brought to you by <a href="https://bitbucket.org/inclinescene/public">Incline</a></li>
<li>Code by <a href="http://www.devwrench.com">cxw</a></li>
<li>Hosted by <a href="http://planet-d.net">Guardian</a></li>
<li>Theme <a href="https://github.com/cxw42/startbootstrap-sb-admin">here</a></li>
<li>Theme based on <a href="https://startbootstrap.com/template-overviews/sb-admin/">SB Admin</a>
by <a href="http://davidmiller.io/">David Miller</a></li>
</ul>
EOCREDZ
            ],

            // no crumbs
        ]; //$vars

        $this->response->getBody()->write(
            $this->twig->render($this->TMPL_ROOT, $vars)
        );
    } //credz()

    /**
     * @route /greetz
     */
    function greetz() {
        $vars = [
            'title' => 'Greetings - Howto',
            'nosearch' => true,

            'content' => [
                'title' => 'Greetings',
                // no subtitle
            ],

            'rows' => [
                '<h4>Greetings in no particular order to:</h4>',
                'Brainstorm',
                'CMUCC',
                'CVGM',
                'drift',
                'ps',
                'and our families!'
            ],

            // no crumbs
        ]; //$vars

        $this->response->getBody()->write(
            $this->twig->render($this->TMPL_ROOT, $vars)
        );
    } //greetz()

} //HApp

(new HApp(SITE_ROOT))->run();

// vi: set ts=4 sts=4 sw=4 et ai: //
